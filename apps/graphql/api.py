from graphene_federation import build_schema

from .projects.schema import ProjectQueries


class Query(ProjectQueries):
    pass


class Mutation:
    pass


schema = build_schema(Query)

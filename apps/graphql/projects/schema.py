import graphene

from apps.projects import models
from .types import Project


class ProjectQueries(graphene.ObjectType):
    project = graphene.Field(
        Project,
        id=graphene.Argument(graphene.ID, description='ID of the Project'),
        slug=graphene.String(description='The slug of the Project.'),
        description='Look up a Project by ID or slug',
    )
    projects = graphene.List(Project)

    def resolve_project(self, info, id=None, slug=None):
        if id:
            return models.Project.objects.filter(id=id).first()
        if slug:
            return models.Project.objects.filter(slug=slug).first()

    def resolve_projects(self, info):
        return models.Project.objects.all()

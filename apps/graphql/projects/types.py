import graphene
from graphene_django import DjangoObjectType

import apps.projects.models as models


class Money(graphene.Scalar):
    @staticmethod
    def serialize(value):
        return value

    @staticmethod
    def parse_literal(node):
        return node

    @staticmethod
    def parse_value(value):
        return value

    @staticmethod
    def resolve(value):
        result = {
            'amount': 0,
            'currency': 'USD',
        }
        if value is None:
            return result

        result['currency'] = str(value.currency)

        try:
            result['amount'] = float(value.amount)
        except ValueError:
            return result

        return result


class Plan(DjangoObjectType):
    class Meta:
        model = models.Plan
        interfaces = (graphene.relay.Node,)
        only_fields = (
            'name',
            'slug',
            'description',
            'is_selected',
            'created',
            'updated',
        )


class Project(DjangoObjectType):
    our_investment = graphene.Field(Money, description="Our investments for the project.", required=False)
    min_deposit = graphene.Field(Money, description="Min deposit for the project.", required=False)
    plans = graphene.List(Plan, description="List of plans for the project.", required=False)

    class Meta:
        model = models.Project
        interfaces = (graphene.relay.Node,)
        only_fields = (
            'id',
            'name',
            'slug',
            'description',
            'our_investment',
            'min_deposit',
            'image',
            'status',
            'withdrawal',
            'url',
            'plans',
            'created',
            'updated',
        )

    def resolve_our_investment(self, info):
        return Money.resolve(self.our_investment)

    def resolve_min_deposit(self, info):
        return Money.resolve(self.min_deposit)

    def resolve_plans(self, info):
        return models.Plan.objects.filter(project=self.id)

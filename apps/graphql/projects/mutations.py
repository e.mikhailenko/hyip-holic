import graphene


class BaseInput(graphene.InputObjectType):
    name = graphene.String(description='Name of the model')
    slug = graphene.String(description='Slug of the model')
    description = graphene.String(description='Description of the model')


class ProjectInput(BaseInput):
    pass

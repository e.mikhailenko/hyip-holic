from django.core.validators import FileExtensionValidator
from django.db import models
from djmoney.models.fields import MoneyField


def project_images_directory_path(instance, filename):
    return f'project_images/{instance.id}/{filename}'


class BaseModel(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200)
    description = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.name

    class Meta:
        abstract = True


class Status(BaseModel):
    class Meta:
        verbose_name_plural = "statuses"


class Withdrawal(BaseModel):
    class Meta:
        verbose_name_plural = "withdrawals"


class Project(BaseModel):
    our_investment = MoneyField(
        decimal_places=2,
        default=0,
        default_currency='USD',
        max_digits=11,
    )
    min_deposit = MoneyField(
        decimal_places=2,
        default=0,
        default_currency='USD',
        max_digits=11,
    )
    image = models.ImageField(
        upload_to=project_images_directory_path,
        validators=[FileExtensionValidator(['jpg', 'jpeg', 'png'])],
        blank=True,
        null=True
    )
    status = models.ForeignKey(
        Status,
        related_name='projects',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    withdrawal = models.ForeignKey(
        Withdrawal,
        related_name='projects',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    url = models.URLField(max_length=500)


class Plan(BaseModel):
    is_selected = models.BooleanField()
    min_deposit = MoneyField(
        decimal_places=2,
        default=0,
        default_currency='USD',
        max_digits=11,
    )
    project = models.ForeignKey(
        Project,
        related_name='plans',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )


class Profit(BaseModel):
    deposit = MoneyField(
        decimal_places=2,
        default=0,
        default_currency='USD',
        max_digits=11,
        blank=False,
        null=False
    )
    profit_amount = MoneyField(
        decimal_places=2,
        default=0,
        default_currency='USD',
        max_digits=11,
        blank=False,
        null=False
    )
    profit_percent = models.DecimalField(
        default=0,
        max_digits=10,
        decimal_places=2
    )
    cryptocurrency = models.CharField(max_length=20)
    project = models.ForeignKey(
        Project,
        related_name='profit',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    plan = models.ForeignKey(
        Plan,
        related_name='profit',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

    def save(self, *args, **kwargs):
        if self.profit_percent == 0:
            try:
                self.profit_percent = (self.profit_amount/self.deposit) * 100
            except:
                self.profit_percent = 0
        super(Profit, self).save(*args, **kwargs)

from django import forms
from django.contrib import admin

from .models import Project, Status, Withdrawal, Plan, Profit


class ProfitInlineFormAdmin(forms.ModelForm):
    class Meta:
        model = Profit
        fields = [
            'name',
            'deposit',
            'profit_amount',
            'profit_percent',
            'cryptocurrency',
            'project',
            'plan'
        ]

    def __init__(self, *args, **kwargs):
        super(ProfitInlineFormAdmin, self).__init__(*args, **kwargs)
        if self.instance.project and self.instance.project.id:
            projects = self.instance.project.plans.all()
            self.fields['plan'].queryset = projects


class PlanInline(admin.TabularInline):
    model = Plan
    extra = 1
    show_change_link = True
    readonly_fields = ['description']
    prepopulated_fields = {'slug': ('name',), }


class ProfitInline(admin.TabularInline):
    form = ProfitInlineFormAdmin
    model = Profit
    fields = ['name', 'slug', 'deposit', 'profit_amount', 'profit_percent', 'cryptocurrency', 'plan']
    extra = 1
    show_change_link = True
    readonly_fields = ['description']
    prepopulated_fields = {'slug': ('name',), }


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ['name', 'our_investment', 'min_deposit', 'status', 'withdrawal',
                    'url', 'created', 'updated']
    list_filter = ['created', 'updated']
    search_fields = ['name']
    inlines = [PlanInline, ProfitInline]
    prepopulated_fields = {'slug': ('name',), }


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    list_display = ['name', 'created', 'updated']
    list_filter = ['created', 'updated']
    search_fields = ['name']
    prepopulated_fields = {'slug': ('name',), }


@admin.register(Withdrawal)
class WithdrawalAdmin(admin.ModelAdmin):
    list_display = ['name', 'created', 'updated']
    list_filter = ['created', 'updated']
    search_fields = ['name']
    prepopulated_fields = {'slug': ('name',), }


@admin.register(Plan)
class PlanAdmin(admin.ModelAdmin):
    list_display = ['name', 'project', 'min_deposit', 'is_selected', 'created', 'updated']
    list_filter = ['created', 'updated']
    search_fields = ['name']
    prepopulated_fields = {'slug': ('name',), }


@admin.register(Profit)
class ProfitAdmin(admin.ModelAdmin):
    # form = ProfitInlineFormAdmin
    list_display = [
        'name',
        'deposit',
        'profit_amount',
        'profit_percent',
        'project',
        'plan',
        'created',
        'updated'
    ]
    list_filter = ['created', 'updated']
    search_fields = ['name']
    prepopulated_fields = {'slug': ('name',), }
